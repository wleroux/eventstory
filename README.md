Event Story
===

Event Story is a rudimentary event store written in golang. It provides an easy-to-use interface for creating products that use Event Sourcing.

Events created with Event Story are currently retained exclusively in-memory.

Configuration
---

The `EVENT_STORY_HTTP_PORT` environment variable is used to determine the http port. By default, the port is 80.

Adding Events
---

To create an event:

POST http://service:port/[topic]

If the topic does not exist, one will be created.

The retrieved event will use the same Content-Type as the one provided by the creation request.

Retrieving Events
---

To retrieve an event:

GET http://service:port/[topic]?offset=[offset]

* offset: the offset of the event you would like to retrieve
