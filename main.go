package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
)

// Event represents a single instance of an event in a topic
type Event struct {
	ContentType string
	Content     []byte
}

// Topic represents the history of all events of a topic
type Topic struct {
	Events []Event
}

var topics = make(map[string][]Event)

func handler(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		topic := r.URL.Path[1:]
		if len(topic) <= 0 {
			w.WriteHeader(http.StatusBadRequest)
			fmt.Fprintf(w, "Topic must be specified")
			return
		}

		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			fmt.Fprintf(w, "Event must has content")
			return
		}

		if topics[topic] == nil {
			topics[topic] = make([]Event, 0)
		}
		topics[topic] = append(topics[topic], Event{
			ContentType: r.Header.Get("Content-Type"),
			Content:     body,
		})

		w.WriteHeader(http.StatusOK)
	} else if r.Method == "GET" {
		topic := r.URL.Path[1:]

		offsetString := r.URL.Query().Get("offset")
		if offsetString == "" {
			offsetString = "0"
		}

		offset, err := strconv.Atoi(offsetString)
		if err != nil || offset < 0 {
			w.WriteHeader(http.StatusBadRequest)
			fmt.Fprintf(w, "Offset must be a valid positive integer")
			return
		}

		events := topics[topic]
		if events == nil || len(events) <= offset {
			w.WriteHeader(http.StatusNotFound)
			w.Write([]byte{})
			return
		}

		event := events[offset]
		w.Header().Set("Content-Type", event.ContentType)
		w.Header().Set("X-Event-Topic", topic)
		w.Header().Set("X-Event-Index", strconv.Itoa(offset))
		w.Header().Set("Cache-Control", "public")
		w.WriteHeader(http.StatusOK)
		w.Write(event.Content)
	}
}

func main() {
	http.HandleFunc("/", handler)

	port := os.Getenv("PORT")
	if len(port) == 0 {
		port = "80"
	}

	http.ListenAndServe(":"+port, nil)
}
